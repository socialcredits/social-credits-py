#!/usr/bin/env python
# -*- coding:utf-8 -*-

"""
import date_extractor
import logger
#import request_util
import xawesome_codechecker
import xawesome_location
import xawesome_mail
import xawesome_time
import xawesome_util
import xawesome_crawler
#import xawesome_mq
#import xawesome_db
import errors
"""

__version__ = '1.3.5'

__all__ = [
    'logger',
    'xawesome_codechecker',
    'xawesome_location',
    'xawesome_mail',
    'xawesome_time',
    'xawesome_util',
    'xawesome_crawler',
    'errors',
    'kv_helper',
    'name_extraction',
]


_URL = 'https://git.oschina.net/socialcredits/social-credits-py/raw/master/scpy/__init__.py'
_VERSION_PATTERN = ur"__version__.+?['\"](.+)['\"]"
_WARNING_TEXT = '\n\n发现新版本[%s]， 当前版本[%s]。\n\n请使用[pip install git+https://git.oschina.net/socialcredits/social-credits-py --upgrade]安装新版本。\n\n'

try:
    import urllib2 as _urllib2, re as _re
    _new_version = _re.findall(_VERSION_PATTERN, _urllib2.urlopen(_URL, timeout=0.5).read())[0]
    if _new_version != __version__:
        import warnings as _warnings
        _warnings.warn(_WARNING_TEXT % (_new_version, __version__))
except Exception, e:
    pass
