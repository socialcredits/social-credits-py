# -*- coding:utf-8 -*- 

import os
from datetime import date

PATH="py_log/"

def assure_path_exists(path):
        dir = os.path.dirname(path)
        if not os.path.exists(dir):
                os.makedirs(dir)

assure_path_exists(PATH)

SC_LOGGING_CONF = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "sc": {
            "format": "[%(asctime)s] [%(levelname)s] [%(threadName)s:%(thread)d] [%(filename)s:%(lineno)d] [%(scid)s] - %(message)s"
        },
        "other": {
            "format": "[%(asctime)s] [%(levelname)s] [%(threadName)s:%(thread)d] [%(filename)s:%(lineno)d] [] - %(message)s"
        }

    },

    "handlers": {
        "sc_console_handler": {
            "class": "logging.StreamHandler",
            "level": "INFO",
            "formatter": "sc",
            "stream": "ext://sys.stdout"
        },
        "sc_file_handler": {
            "class": "logging.handlers.RotatingFileHandler",
            "level": "INFO",
            "formatter": "sc",
            "filename": PATH + "sc-" + date.today().isoformat() + ".log",
            "maxBytes": 10485760,
            "backupCount": 20,
            "encoding": "utf8"
        },
        "other_console_handler": {
            "class": "logging.StreamHandler",
            "level": "WARNING",
            "formatter": "other",
            "stream": "ext://sys.stdout"
        },
        "other_file_handler": {
            "class": "logging.handlers.RotatingFileHandler",
            "level": "WARNING",
            "formatter": "other",
            "filename": PATH + "sc-" + date.today().isoformat() + ".log",
            "maxBytes": 10485760,
            "backupCount": 20,
            "encoding": "utf8"
        },

    },
    'loggers': {
        # "sc": {
        #     "level": "INFO",
        #     "handlers": ["sc_console_handler", "sc_file_handler"]
        # },
        "": {
            "level": "WARNING",
            "handlers": ["other_console_handler", "other_file_handler"]
        }
    }
    
}
