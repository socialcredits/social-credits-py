#social-credits-py

[gitee](https://gitee.com/socialcredits/social-credits-py "原oschina链接地址")

---

### date_extractor：从字符串中取时间

所有方法都返回datetime.datetime 对象，如果没找到，返回None,下面是例子

```
from scpy.date_extractor import extract_chinese_date,format_date_str,extract_first_date
```
>中文日期
```python
test_1 = u'我是测试字符串网二〇一五年七月十五日中国共产'
print extract_chinese_date(test_1)
```

>从字符串中取各种时间
```python
test_2 = u'2014年12月23日法范德萨'
print extract_first_date(test_2)
```

>主要格式化数字不是两位和时间缺失或部分缺失,分隔符需要是'-' 和'：'如果不是请使用test_2
```python
test_3 = '2015-9-7'
print format_date_str(test_3)
```

--- 

###url请求封装

 - RequestUtil 一个全局的requestUtil ，默认会使用同一个request.session来发送请求

```Python

from scpy.request_util import RequestUtil

proxy_config = {
        'ip':u'代理库存放ip',
        'port':27017,   
        'db':u'代理库名',
        'collection':u'代理collection',
    }

request_util = RequestUtil(proxy_config) #proxy_config 缺失，则不使用代理
request_util.make_request(url)    #默认为get请求默认isproxy=False 不使用代理
```
   

---

### xawesome_time: 时间格式化

#####功能： 格式化时间字符串为标准格式
 - 标准格式：
     > "现在是2012-10-02 02:01:01"将返回`2012-10-02 02:01:01`


 - 普通格式：
     > "现在是2012-10-02"将返回`2012-10-02 00:00:00`
   "现在是2012年10月02日"将返回`2012-10-02 00:00:00`
   "现在是2012年10月02日13时27分4秒"将返回`2012-10-02 03:27:04`
   ......

 - 中文格式（仅支持到日，时分秒中文格式暂不支持，由于实际意义不大，可预见的将来也不会支持）：
     > "现在是二〇一五年十二月二十五日"将返回`2015-12-25 00:00:00`

 - 时间表达式：
     > "3天后" 将返回距离调用时间三天之后的时间的字符串表示
   同理，可支持： "2年前", "1个季度之后", "5天以前", "3小时后", ...
      

#####用法：

```Python
from scpy import xawsome_time

xawsome_time.parse_time(TIME_STR)
```

 - 参数： 
     > time_str: [str, unicode] 时间字符串
   convert_2_time_obj: [bool] True => 转换为对象 | False => 转换为字符串 | default value: False


---

### xawesome_mail: 发送邮件

#####功能： 发送提醒邮件邮件

 **注意**: 应用场景为程序运行过程中的提醒邮件，不可用于大量发送邮件的场景。


#####用法：

```Python
from scpy import xawsome_mail

xawsome_mail.send_mail('中文主题', u'中文正文', ['xu.du@socialcredits.cn'], sender_nickname='发信人')
```

 - 参数： 
```
    subject    : 邮件主题
    content    : 邮件正文
    mailto_list: 邮件接收者列表
    file_path  : 附件路径（没有则为空）
    kwargs: 关键字参数（ATT: 后面三个参数必须同时制定或者同时不指定）：
                sender_nickname ： 发件人昵称
                sender_mail_addr： 定制发件人邮件地址
                sender_mail_pass： 定制发件人邮箱密码
                sender_mail_host： 定制发件人邮箱服务器
```     
     
---

### xawesome_location: 位置相关服务

#####用法：

```Python
from scpy.xawesome_location import parse_location

parse_location(u'杭州誉存科技有限公司')

>>> {"province": "浙江", "cityShortName": "杭州", "cityName": "杭州市", "id": "3301"}
```

 - 参数： 
    company_name : 公司全称
 - 返回：
    {"id": "地区编号", "cityName": u"城市全称", "province": u"省份", "cityShortName": "城市简称"}


---

### xawesome_codechecker

#### 测试函数运行时间 @timeit 

```Python
from scpy.xawesome_codechecker import timeit
@timeit
def func():
    pass

>>>func()
function [func] start at [2015-11-03 15:04:49]
function [func] exit  at [2015-11-03 15:04:49]
function [func] coast [0.0350475311279ms]
```

#### 控制函数不抛出异常 @no_exception
 - 有时候不希望代码在某一句停下，但每句代码 `try-catch` 又很难看，可以使用 `@no_exception` 控制函数在遇到异常时返回某默认值。

#### 获取程序运行的主机信息
 - 程序可能运行在开发机/测试机/线上或其他环境，在发警告邮件时不容易区分来自哪里，可使用`get_user()`获取当前运行程序的用户名， `get_ip()`获取本机IP， `get_runner()`获取类似`user@host`格式的信息。

---

### xawesome_util

 - 建议通过这里导入`BeautifulSoup`，会自动检测并使用系统已安装的html parser。
 - url 编码解码工具。


---

### kv_helper: JSON变换 & 键值处理服务

#####用法：
**get_hierachical_kv**(content, key_list, default={}):

 - content: 用来操作的json字典，必须
 - key_list: 查询的key的序列，必须
 - default: 查不到或查到最后一层的值为[]|{}|None时返回的默认值，非必须

```Python
from scpy.kv_helper import get_hierachical_kv
tmp_dict = {'a1':{'b1':{'c1':[], 'c2':'天空'}}}

get_hierachical_kv(tmp_dict, ['a1', 'b1', 'c1'], 'my_default_value')
>>> my_default_value

get_hierachical_kv(tmp_dict, ['a1', 'b1', 'c2'], 'my_default_value')
>>> 天空

get_hierachical_kv(tmp_dict, ['a1', 'b1', 'c2', 'd1'], {'key':'name', 'value':None})
>>> {'key':'name', 'value':None}
```

**set_hierachical_kv**(content, key_list, value):

 - content: 用来操作的json字典，必须
 - key_list: 查询的key的序列，必须
 - value: 赋值内容，必须

```Python
from scpy.kv_helper import set_hierachical_kv
tmp_dict = {}

set_hierachical_kv(tmp_dict, ['a1', 'b1'], 'lalala')
>>> {'a1': {'b1': 'lalala'}}

set_hierachical_kv(tmp_dict, ['a1', 'b2'], 'apple')
>>> {'a1': {'b1': 'lalala', 'b2': 'apple'}}

set_hierachical_kv(tmp_dict, ['a1', 'b2', 'c1'], 'banana')
>>> {'a1': {'b1': 'lalala', 'b2': {'c1': 'banana'}}}

set_hierachical_kv(tmp_dict, ['a1'], ['orange', 'barry'])
>>> {'a1': ['orange', 'barry']}
```

**trans_json**(json_data, encoding='unicode'):

 - json_data: 需要统一转化编码的json数据(泛指广义json, 包含dict|list|数值|字符串)
 - encoding: 需要转化成的编码类型, 默认**unicode**, 
     - 支持utf-8|gb18030|gb2312|gbk|big5 
     - 实测中big5转化偶尔会因字符无法对应而报错, json中造成报错的部分会以**None**作为对应值返回

```Python
from scpy.kv_helper import trans_json

json_data = {'key1': '我是utf-8编码', 'key2': {'utf-8键3':3.14, u'unicode键4':[u'我是unicode', '我是utf-8'], 5:None}}
trans_json(json_data, encoding='unicode')
>>> {u'key1': u'\u6211\u662futf-8\u7f16\u7801',
 u'key2': {5: None,
  u'unicode\u952e4': [u'\u6211\u662funicode', u'\u6211\u662futf-8'],
  u'utf-8\u952e3': 3.14}}

trans_json(json_data, encoding='utf-8')
>>> {'key1': '\xe6\x88\x91\xe6\x98\xafutf-8\xe7\xbc\x96\xe7\xa0\x81',
 'key2': {5: None,
  'unicode\xe9\x94\xae4': ['\xe6\x88\x91\xe6\x98\xafunicode',
   '\xe6\x88\x91\xe6\x98\xafutf-8'],
  'utf-8\xe9\x94\xae3': 3.14}}
```

**reduce_by_key**(pair_list):

 - pair_list: list of (key,value) pairs
     - key, 任意类型
     - value, 对应同一个key的所有value必须是**可加**的，即同为字符类型或同为数值类型

```Python
from scpy.kv_helper import reduce_by_key

pair_list = [('a', 1), ('a', 3.2), ('b', 'str1'), ('b', 'str2')]
reduce_by_key(pair_list)
>>> [('a', 4.2), ('b', 'str1str2')]
```

**group_by_key**(pair_list):

 - pair_list: list of (key,value) pairs
     - key, 任意类型
     - value, 任意类型

```Python
from scpy.kv_helper import group_by_key

pair_list = [('a', 1), ('a', 3.2), ('a', [u'我不合群']), ('b', 'str1'), ('b', 'str2'), ('b', 3.2)]
group_by_key(pair_list)
>>> [('a', [1, 3.2, [u'\u6211\u4e0d\u5408\u7fa4']]), ('b', ['str1', 'str2', 3.2])]
```
