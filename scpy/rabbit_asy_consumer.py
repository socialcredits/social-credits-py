#!/usr/bin/env python
# -*- coding:utf-8 -*-

from scpy.logger import get_logger
import pika

logger = get_logger(__file__)

class AsyConsumer(object):

    QUEUE = ''

    def __init__(self, amqp_url, queue_name=''):

        if not self.QUEUE:
            if not queue_name:
                raise Exception('no queue specify')
            self.QUEUE = queue_name


        self._connection = None
        self._channel = None
        self._closing = False
        self._consumer_tag = None
        self._url = amqp_url


    def connect(self):
        return pika.SelectConnection(pika.URLParameters(self._url),
                                     self.on_connection_open,
                                     stop_ioloop_on_close=False)

    def on_connection_open(self, unused_connection):
        self.add_on_connection_close_callback()
        self.open_channel()

    def add_on_connection_close_callback(self):
        self._connection.add_on_close_callback(self.on_connection_closed)

    def on_connection_closed(self, connection, reply_code, reply_text):
        self._channel = None
        if self._closing:
            self._connection.ioloop.stop()
        else:
            self._connection.add_timeout(5, self.reconnect)

    def reconnect(self):
        self._connection.ioloop.stop()
        if not self._closing:
            self._connection = self.connect()
            self._connection.ioloop.start()

    def open_channel(self):
        self._connection.channel(on_open_callback=self.on_channel_open)

    def on_channel_open(self, channel):
        self._channel = channel
        self.add_on_channel_close_callback()

        self.setup_queue(self.QUEUE)

    def add_on_channel_close_callback(self):
        self._channel.add_on_close_callback(self.on_channel_closed)

    def on_channel_closed(self, channel, reply_code, reply_text):
        self._connection.close()

    def setup_queue(self, queue_name):
        self._channel.queue_declare(self.on_queue_declareok, queue_name, durable=True)

    def on_queue_declareok(self, method_frame):
        self.start_consuming()

    def start_consuming(self):
        self.add_on_cancel_callback()
        self._consumer_tag = self._channel.basic_consume(self.on_message,
                                                         self.QUEUE, no_ack=True)

    def add_on_cancel_callback(self):
        self._channel.add_on_cancel_callback(self.on_consumer_cancelled)

    def on_consumer_cancelled(self, method_frame):
        if self._channel:
            self._channel.close()

    def on_message(self, unused_channel, basic_deliver, properties, body):
        """ consumer 可以重写这个方法

        如果要实现no_ack 可以重写 start_consuming 方法
        """
        raise  NotImplementedError


    def acknowledge_message(self, delivery_tag):

        self._channel.basic_ack(delivery_tag)

    def stop_consuming(self):
        if self._channel:
            self._channel.basic_cancel(self.on_cancelok, self._consumer_tag)


    def on_cancelok(self, unused_frame):
        self.close_channel()

    def close_channel(self):
        self._channel.close()

    def run(self):
        self._connection = self.connect()
        self._connection.ioloop.start()

    def stop(self):
        self._closing = True
        self.stop_consuming()
        self._connection.ioloop.start()


    def close_connection(self):
        self._connection.close()



if __name__ == '__main__':

    import json
    amqp_url = 'amqp://sc-admin:1qaz2wsx@192.168.31.114:5672/%2F?connection_attempts=3&heartbeat_interval=3600'
    class MongoConsumer(AsyConsumer):
        def on_message(self, unused_channel, basic_deliver, properties, body):
            print json.loads(body)

    consumer = MongoConsumer(amqp_url, queue_name='test')
    print consumer.QUEUE
    try:
        consumer.run()
    except KeyboardInterrupt:
        consumer.stop()