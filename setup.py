#!/usr/bin/env python
# -*- coding:utf-8 -*-

import re
from distutils.core import setup

with open('scpy/__init__.py', 'r') as fp:
    VERSION = re.findall(ur"__version__.+?['\"](.+)['\"]", fp.read())[0]

requires = [
   # 'beautifulsoup4',
    #'stompest',
    #'pymongo',
    #'requests',
    #'Twisted',
    # 'tornado'
]

setup(
    name='scpy',
    version=VERSION,
    description='SocialCredits python lib',
    url='https://git.oschina.net/socialcredits/social-credits-py',
    author='SocialCredits',
    author_email='kai.yan@socialcredits.cn',
    license='MIT',
    # package_dir={'scpy': ''},
    install_requires=requires,
    packages=['scpy'],
)