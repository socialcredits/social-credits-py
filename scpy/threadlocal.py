import threading
threadlocal = threading.local()    

def set_threadlocal(varname, value=""):
    setattr(threadlocal, varname, value)

def get_threadlocal(varname, default=""):
    v = getattr(threadlocal, varname, default)
    return v
